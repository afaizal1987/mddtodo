import React, { useState, useContext, useEffect } from 'react';

// Props hold all actions
const Login = (props) => {


  useEffect(() => {
    
    // eslint-disable-next-line
  }, []);

  const [user, setUser] = useState({
    username: '',
    password: ''
  });

  const { username, password } = user;

  const onChange = e => setUser({ ...user, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    
  };

  return (
    <div className='form-container'>
      <form onSubmit={onSubmit}>
        <div className='form-group'>
          <label htmlFor='username'>username Address</label>
          <input
            id='username'
            type='text'
            name='username'
            value={username}
            onChange={onChange}
            required
          />
        </div>
        <div className='form-group'>
          <label htmlFor='password'>Password</label>
          <input
            id='password'
            type='password'
            name='password'
            value={password}
            onChange={onChange}
            required
          />
        </div>
        <input
          type='submit'
          value='Login'
          className='btn btn-primary btn-block'
        />
      </form>
    </div>
  );
};

export default Login;