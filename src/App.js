import './App.css';
import React, {Fragment} from 'react'
// Routes has now been replaced with Routess
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import Login from './components/pages/Login.js'
import Dashboard from './components/pages/Dashboard.js'

/**
 * Main page
 */
function App() {
  return (
    <Router>
    <Fragment>
      {/* <Navbar/> */}
      <div className='container'>
      {/* <Alerts/> */}
      <Routes>
        <Route exact path="/" element={<Dashboard/>}/>
        <Route exact path="/login" element={<Login/>}/>
      </Routes>  
      </div>
    </Fragment>
  </Router>
    
  );
}

export default App;
